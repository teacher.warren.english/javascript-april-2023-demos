// Get DOM Elements

const btnGetCoffees = document.getElementById("btn-get-coffees")
const descriptionElement = document.getElementById("coffee-description")
const priceElement = document.getElementById("coffee-price")
const idElement = document.getElementById("coffee-id")
const coffeeListElement = document.getElementById("coffee-list")

// Event Listeners
btnGetCoffees.addEventListener('click', getDataAsync)

const apiUrl = "https://thoughtful-vagabond-fibre.glitch.me/coffee"
let coffees = []

// Event Handlers

async function getDataAsync() {
    try {
        const response = await fetch(apiUrl)
        const json = await response.json()
        console.log(json)
        coffees = json
    }
    catch (error) {
        console.error(error.message)
    }
    renderOneCoffee()
    renderCoffees()
}

// Functions

function renderOneCoffee() {
    descriptionElement.innerText = coffees[0].description
    priceElement.innerText = coffees[0].price
    idElement.innerText = coffees[0].id
}

function renderCoffees() {
    coffeeListElement.innerHTML = ''

    for (const coffee of coffees) {
        const newCoffeeElement = document.createElement('li')
        newCoffeeElement.innerText = `${coffee.id} ${coffee.description} ${coffee.price}`

        coffeeListElement.appendChild(newCoffeeElement)
    }
}