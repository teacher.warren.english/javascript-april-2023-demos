const apiUrl = "https://thoughtful-vagabond-fibre.glitch.me/coffee"

// Promise syntax

function getDataSync() {
    fetch(apiUrl)
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(json => console.log(json))
        .catch(error => console.error(error.message))
}


// Async & Await syntax

async function getDataAsync() {
    try {
        const response = await fetch(apiUrl)
        const json = await response.json()
        console.log(json)
    }
    catch (error) {
        console.error(error.message)
    }
}

getDataSync()
// getDataAsync()