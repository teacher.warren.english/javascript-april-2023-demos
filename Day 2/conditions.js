// IFs

const assignmentGrade = 49.9

if (assignmentGrade >= 75) {
    // console.log("Distinction!");
} else if (assignmentGrade >= 50) {
    // console.log("You passed!");
} else {
    // console.log("You failed 🥲");
}

// NESTING (is possible, but avoid it - it takes a lot more processing power)

if (true) {
    console.log("Level 1");
    if (true) {
        console.log("Inception");
        if (false) {
            // nothing happens
        }
    }
}

const name = "Saju"

if (name === "Warren") {
    // console.log("Your name is Warren!");
    // console.log("This is another line of code to execute")
}

// SWITCH

switch (name) {
    case "Warren":
        greetWarren()
        break;
    case "Sean":
        greetSean()
        break;
    case "Dean":
        greetDean()
        break;
    default:
        greetNobody()
}


function greetWarren() {
    console.log("Hei Warren");
}

function greetSean() {
    console.log("Welcome Sean");
}

function greetDean() {
    console.log("Hi Dean!");
}

function greetNobody() {
    console.log("🦗🦗🦗");
}

console.log("End of the script");