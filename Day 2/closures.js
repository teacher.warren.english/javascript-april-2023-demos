// Closure

function outerFunc(name, surname) {

    let greeting = "Hi"

    function innerFunc() {
        let number = 42
        console.log(greeting + " " + name + " " + surname);
    }

    return innerFunc
}

const returnFunc = outerFunc("Warren", "West")

returnFunc()
returnFunc()
returnFunc()
returnFunc()