// HOFs - A function that invokes another function

const numbers = [13, 15, -1, -97, 50, 0, 22, -36]

// .filter()

const evenNumbers = numbers.filter(num => num === 0)
console.log("Filter", evenNumbers);


// .reduce()
const reducedArray = numbers.reduce((previous, current) => previous - current, 0)
console.log("Reduce", reducedArray);


// .map()
const increasedNumbers = numbers.map(x => x + 10)
console.log("Map", increasedNumbers);