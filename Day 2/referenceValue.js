// PRIMITIVES ARE PASSED BY VALUE

const myNumber = 42
let anotherNumber = myNumber

anotherNumber += 10

// console.log(myNumber); // 42
// console.log(anotherNumber); // 52


// OBJECTS ARE PASSED BY REFERENCE

const myObject = {name: "Warren", surname: "West"}
let anotherObject = myObject

anotherObject.name = "Sean"

// console.log("myObject", myObject); // Warren West
// console.log("anotherObject", anotherObject); // Sean West


// =============================================================


const myName = "Warren West" // by val
const myPerson = {name: "Sean", surname: "Skinner"} // by ref

function changeStuff(nameArgument, personArgument) {
    nameArgument += " Jnr"
    personArgument.title = "Mr."
}

changeStuff(myName, myPerson)

console.log(myName); // ?
console.log(myPerson); // ?
