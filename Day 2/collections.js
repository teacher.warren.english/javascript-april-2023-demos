const myArray = [46, 35, 24, 13, 57 ,68, 88]

// console.log(myArray)
// console.log(myArray.length);

const arrayLength = myArray.length

let firstNum = myArray[0]
let secondNum = myArray[1]
let lastNum = myArray[arrayLength - 1]
let eighthNum = myArray[arrayLength]

// console.log("first", firstNum);
// console.log("second", secondNum);
// console.log("last", lastNum);
// console.log("out of bounds", eighthNum); // undefined


// ARRAY HELPER FUNCTIONS
const newLength = myArray.push(100)

const poppedItem = myArray.pop()

// console.log(newLength);
// console.log(poppedItem);
// console.log(myArray);

const fullname = "Warren West"
const middleName = "Thomas"

const firstArray = fullname.split(' ')
const secondArray = middleName.split('')

// console.log(firstArray);
// console.log(secondArray);

const reunited = secondArray.join('')
// console.log(reunited);

const myNumberArray = [1, 2, 3, 4]
const joinedNumbers = myNumberArray.join('')

// console.log(joinedNumbers);


const animals = ["🐈", "🐥", "🦍"]

// .slice()

const newAnimals = animals.slice(0, 2) // shallow

newAnimals.push("🐬")

// console.log(animals); // ?
// console.log(newAnimals); // ?

// .splice()

const forest = ["🌲", "🌲", "🌲", "🌲"]
forest.splice(2, 2, "🌱")

console.log(forest);