// Looping - how do we iterate?

const myArray = [13, 15, -1, -97, 50, 0, 22, -36]
const myObject = {description: "The best PC monitor", title: "Sumsang XL", price: 199.99}

// for

let numOfElements = myArray.length

for (let i = 0; i < numOfElements; i++) {
    console.log("for", myArray[i]);
}

// for of
for (const item of myArray) {
    console.log("for...of", item)
}

// for in
for (const key in myObject) {
    console.log("for...in", key);
    console.log(`${myObject[key]}`)
}

// do while
let count = 0

do {
    console.log("do...while", count);
    count++
} while (count < 10)


// while
while (count > 0) {
    console.log("while", count);
    count--
}