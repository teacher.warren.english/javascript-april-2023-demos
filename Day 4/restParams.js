function sum(...numbers) {

    const isValid = true

    for (const num of numbers)
        if (isNaN(num)) {
            console.error("You need to provide numbers");
            isValid = false
        }

    if (isValid) {
        const result = numbers.reduce((prev, current) => {
            return prev + current
        }, 0)

        return result
    }
}

console.log(sum(5, 22, 48, 51, 35, 10, 10, "")); 