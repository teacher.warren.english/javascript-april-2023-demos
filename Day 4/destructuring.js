// Object destructuring

const warren = {
    firstname: "Warren",
    lastname: "West",
    age: 30,
}

const {firstname:name1, lastname:name2} = warren

console.log(`${name1} ${name2}`);

// Array destructuring

const numbers = [13, 57, 24, 50]

const [num1, num2, num3, num4] = numbers

console.log(`${num1} ${num2} ${num3} ${num4} Sum = ${num1 + num2 + num3 + num4}`)

// Assigning multiple values to variables on a single line using destructuring syntax

let [cat, dog, mouse, turtle] = ["Garfield", "Bruno", "Splinter", "Donatello"]

console.log(cat)
console.log(turtle)