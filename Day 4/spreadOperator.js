// Spread Operator (Arrays)

const nums = [1, 5, 34, 16, -9]

let newArray = [5, 10, 15, ...nums, 55, 60, 70] // spread operator (same as .slice())

newArray.push(22)

console.log(nums);
console.log(newArray);



// Spread Operator (Objects)

const lecturer = {fname: "Warren", lname: "West", age: 30}

let newLecturer = {
    ...lecturer,
    supervisor: "Sean",
    pets: ["Bruno", "Garfield"]
}

newLecturer.fname = "Sean"

console.log(lecturer);
console.log(newLecturer);