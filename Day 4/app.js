import bank from './bank.js'
import { COFFEE_API_URL } from './utils.js'

// DOM ELEMENTS:

const btnDeposit = document.getElementById("btn-deposit")
const btnWithdraw = document.getElementById("btn-withdraw")
const bankBalance = document.getElementById("bank-balance")
const btnGetCoffees = document.getElementById("btn-get-coffee")
const coffeeList = document.getElementById("coffee-list")
const coffeeDescription = document.getElementById("coffee-description")
const coffeePrice = document.getElementById("coffee-price")

// VARIABLES
let coffees = []

// EVENT LISTENERS:

btnDeposit.addEventListener('click', () => handleDeposit(100))
btnWithdraw.addEventListener('click', () => handleWithdraw(25))
btnGetCoffees.addEventListener('click', handleGetCoffees)
coffeeList.addEventListener('change', handleSelectedNewCoffee)

// EVENT HANDLERS:

function handleDeposit(amount) {
    // add money to the bank balance
    console.log("Fired deposit");
    bank.deposit(amount)
    bankBalance.innerText = `$${bank.getBalance()}.00`
}

function handleWithdraw(amount) {
    // subtract money from the bank balance
    console.log("Fired withdraw");

    if (bank.getBalance() < amount) {
        alert("Your bank balance is too low 😭")
        return
    }
    bank.withdraw(amount)
    bankBalance.innerText = `$${bank.getBalance()}.00`
}

function handleGetCoffees() {
    fetch(COFFEE_API_URL)
        .then(response => response.json())
        .then(json => {
            coffees = json
            populateCoffeeList()
        })
        .catch(error => console.error(error.message))
}

function handleSelectedNewCoffee(event) {
    let currentCoffee = coffees.find(item => item.id == event.target.value)

    coffeeDescription.innerText = currentCoffee.description
    coffeePrice.innerText = currentCoffee.price
}

// FUNCTIONS

function populateCoffeeList() {
    if (!coffees) {
        console.error("There are no coffees to display! 😭");
        return
    }

    coffeeList.innerHTML = ''
    for (const coffee of coffees) {
        const newCoffee = document.createElement('option')
        newCoffee.innerText = coffee.description
        newCoffee.value = coffee.id
        coffeeList.appendChild(newCoffee)
    }
}

bankBalance.innerText = bank.getBalance()

// let loanAmount = prompt("Enter loan amount")

// if (loanAmount > bank.getBalance() * 2)
//     console.log("That's too high!");
// else
//     console.log("You qualify for the loan!");