let balance = 100 // hidden property

// setters

const deposit = amount => {
    balance += amount
}

const withdraw = amount => {
    balance -= amount
}

// getter

const getBalance = () => {
    return balance
}

const bank = {
    deposit,
    withdraw,
    getBalance
}

export default bank