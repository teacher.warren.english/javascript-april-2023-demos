class Animal {
    constructor(name, emoji) {
        this.name = name
        this.emoji = emoji
    }

    // methods
    displayInfo() {
        console.log(`Name: ${this.name} | Emoji: ${this.emoji}`);
    }
}

export class Cat extends Animal {
    
    livesRemaining = 9
    favoriteFood = "Tuna"
    
    constructor(name, emoji, purrVolume) {
        super(name, emoji)
        this.purrVolume = purrVolume
    }

    chasedByCats() {
        console.log(`${this.name} is being chased by cats`);
    }

    purr() {
        switch (this.purrVolume) {
            case 1:
                console.log(`${this.name} is purring quietly`);
                break
            case 2:
                console.log(`${this.name} is purring happily`);
                break
            case 3:
                console.log(`${this.name} is purring loudly`);
                break
            default:
                console.log(`${this.name} is purring normally`);
                break
        }
    }

    // override method
    displayInfo() {
        console.log(`Name: ${this.name} | Emoji: ${this.emoji} | Purr Volume: ${this.purrVolume}`)
    }
}

export default class Mouse extends Animal {

    #disease = "DIVOC"
    location = "Oslo"

    constructor (name, emoji) {
        super(name, emoji)
    }

    eatCheese() {
        console.log("This cheese is lovely! 🧀");
    }

    updateDiseaseStatus(newStatus) {
        this.#disease = newStatus
    }

    printDisease() {
        console.log(this.#disease);
    }

    #mutateDisease() {
        console.log(`The ${this.#disease} disease is mutating!`);
    }
}
