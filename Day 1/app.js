// const addTax = function (coffeePrice) {
//     return coffeePrice * 1.25;
// }

// const addTax = coffeePrice => coffeePrice * 1.25;

// console.log(addTax(10));

// const addTwoNumbers = (num1, num2) => num1 + num2

// console.log(addTwoNumbers(5,3));

// Objects

// const dog = {
//     // props
//     weight: 50,
//     color: "orange",
//     name: "Ruffles",

//     // methods
//     greeting() {
//         console.log(this.name + " says hi!");
//     }
// }

// dog.greeting();
// console.log("Dog's color is: ",dog.color);


// Functional Constructor

// function Animal(weight, color, name) {
//     this.weight = weight;
//     this.color = color;
//     this.name = name;
// }

// const dog = new Animal(50, "orange", "Ruffles");
// const cat = new Animal(5, "white", "Ghost");
// const fish = new Animal(0.05, "silver", "Blubbles");

// console.log("Dog: ", dog, "Cat: ", cat, "Fish: ", fish);


// function foo() {
//     console.log(baz);
// }

// function bar() {
//     let baz = "BAZ2"
//     foo()
// }

// let baz = "BAZ1"
// bar() // BAZ1

let myVar = "var1"

if (true) {
    let myVar2 = "var2"
}

console.log(myVar2) // ?